package com.telematics.emergencyattendant.entities;

import java.io.Serializable;
import java.util.Date;

public class Report implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String code;
	private String type;
	private Date time;
	private double latitude;
	private double longitude;

	public Report(String code, String type, Date time, double latitude, double longitude) {
		this.code = code;
		this.type = type;
		this.time = time;
		this.setLatitude(latitude);
		this.setLongitude(longitude);
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

}
