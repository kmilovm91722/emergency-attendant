package com.telematics.emergencyattendant.tasks;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.Vector;

import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import com.telematics.emergencyattendant.R;
import com.telematics.emergencyattendant.entities.Report;
import com.telematics.emergencyattendant.utilities.XMLElement;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.util.Log;

public class WSClientTask extends AsyncTask<Double, Integer, Long> {

	private Activity activity;
	private Report[] reports;
	private boolean complete = false;
	private double latitude;
	private double longitude;
	private ProgressDialog dialog;

	public WSClientTask(Activity activity, double latitude, double longitude) {
		super();
		this.activity = activity;
		this.latitude = latitude;
		this.longitude = longitude;
	}

	public WSClientTask(Activity activity) {
		super();
		this.activity = activity;
	}

	@Override
	protected void onPreExecute() {
		dialog = new ProgressDialog(activity);
		dialog.setMessage(activity.getString(R.string.message_loading));
		dialog.setIndeterminate(true);
		dialog.setCancelable(false);
		dialog.show();
	}

	public String callRESTWebService(final String url)
			throws ClientProtocolException, IOException, OutOfMemoryError {
		String responseString = "";
		Log.d("EmergencyAttendant", "Peticion a :" + url);

		final HttpClient httpclient = new DefaultHttpClient();
		final HttpResponse response = httpclient.execute(new HttpGet(url));
		final StatusLine statusLine = response.getStatusLine();

		if (statusLine.getStatusCode() == HttpStatus.SC_OK) {
			final ByteArrayOutputStream out = new ByteArrayOutputStream();
			response.getEntity().writeTo(out);
			out.close();
			responseString = out.toString();
			// ..more logic
		} else {
			// Closes the connection.
			response.getEntity().getContent().close();
			throw new IOException(statusLine.getReasonPhrase());
		}

		return responseString;
	}

	private String[] getCodes(Activity activity, double lat, double lng)
			throws ClientProtocolException, OutOfMemoryError, IOException {
		String webServiceResult = callRESTWebService((activity
				.getString(R.string.web_service_get_codes))
				+ "?latitude="
				+ lat + "&longitude=" + lng);
		XMLElement xmlElement = new XMLElement();
		xmlElement.parseString(webServiceResult);
		Vector<Object> results = xmlElement.getChildren();

		String[] codes = new String[results.size()];

		for (int i = 0; i < results.size(); i++) {
			codes[i] = (String) ((XMLElement) results.get(i)).getContent();
		}
		return codes;
	}

	private Report[] getReports(Activity activity)
			throws ClientProtocolException, OutOfMemoryError, IOException {
		String webServiceResult = callRESTWebService((activity
				.getString(R.string.web_service_get_reports)));
		XMLElement xmlElement = new XMLElement();
		xmlElement.parseString(webServiceResult);
		Vector<Object> results = xmlElement.getChildren();

		Report[] reports = new Report[results.size()];

		for (int i = 0; i < results.size(); i++) {
			String code = (String) ((XMLElement) results.get(i))
					.getAttribute("code");
			String type = (String) ((XMLElement) results.get(i))
					.getAttribute("type");
			Date time = new Date(Long.parseLong((String) ((XMLElement) results
					.get(i)).getAttribute("time")));
			double latitude = Double
					.parseDouble(((String) ((XMLElement) results.get(i))
							.getAttribute("latitude")));
			double longitude = Double
					.parseDouble(((String) ((XMLElement) results.get(i))
							.getAttribute("longitude")));
			reports[i] = new Report(code, type, time, latitude, longitude);
		}
		return reports;
	}

	public Report[] getReports() {
		return reports;
	}

	private void setReports(Report[] reports) {
		this.reports = reports;
	}

	public boolean isComplete() {
		return complete;
	}

	public void setComplete(boolean complete) {
		this.complete = complete;
	}

	public Report getReportByCode(String code, Report[] reports) {
		for (int i = 0; i < reports.length; i++) {
			if (reports[i].getCode().equals(code)) {
				return reports[i];
			}
		}
		return null;
	}

	@Override
	protected Long doInBackground(Double... params) {
		try {
			String[] codes = getCodes(activity, this.latitude, this.longitude);
			Report[] results = getReports(activity);
			Report[] reports = new Report[codes.length];
			for (int i = 0; i < reports.length; i++) {
				reports[i] = getReportByCode(codes[i], results);
			}
			setReports(reports);

		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		setComplete(true);
		return null;
	}

	@Override
	protected void onPostExecute(Long result) {
		dialog.dismiss();
	}
}
