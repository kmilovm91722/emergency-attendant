package com.telematics.emergencyattendant.activities;

import java.io.IOException;
import java.util.List;

import org.apache.http.client.ClientProtocolException;

import com.google.android.maps.GeoPoint;
import com.google.android.maps.MapActivity;
import com.google.android.maps.MapView;
import com.google.android.maps.Overlay;
import com.google.android.maps.OverlayItem;
import com.telematics.emergencyattendant.R;
import com.telematics.emergencyattendant.entities.Report;
import com.telematics.emergencyattendant.tasks.WSClientTask;
import com.telematics.emergencyattendant.utilities.MarkerCreator;

import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

public class DetailsActivity extends MapActivity {
	
	private Report report;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_details);
		MapView mapView = (MapView) findViewById(R.id.mapview);
		mapView.setBuiltInZoomControls(true);
		report = (Report) getIntent().getSerializableExtra("report");
		TextView textViewType = (TextView) findViewById(R.id.textViewTypeActivityDetails);
		textViewType.setText(getString(R.string.text_view_type) + ": "
				+ report.getType());
		TextView textViewTime = (TextView) findViewById(R.id.textViewTimeActivityDetails);
		textViewTime.setText(getString(R.string.text_view_time) + ": "
				+ report.getTime().toString());

		List<Overlay> mapOverlays = mapView.getOverlays();
		Drawable drawable = this.getResources().getDrawable(
				R.drawable.ic_launcher);
		MarkerCreator markerCreator = new MarkerCreator(drawable, this);

		GeoPoint point = new GeoPoint((int) (report.getLatitude() * 1e6),
				(int) (report.getLongitude() * 1e6));
		OverlayItem overlayitem = new OverlayItem(point, report.getType(),
				report.getTime().toString());

		markerCreator.addOverlay(overlayitem);
		mapOverlays.add(markerCreator);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_google_maps, menu);
		return true;
	}
	
	public void onAcceptButtonClick(View view){
		WSClientTask client =  new WSClientTask(this);
		try {
			client.callRESTWebService(getString(R.string.web_service_report)+"?code="+report.getCode());
			finish();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (OutOfMemoryError e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	protected boolean isRouteDisplayed() {
		// TODO Auto-generated method stub
		return false;
	}
}
