package com.telematics.emergencyattendant.activities;

import com.telematics.emergencyattendant.R;
import com.telematics.emergencyattendant.entities.Report;
import com.telematics.emergencyattendant.tasks.WSClientTask;
import com.telematics.emergencyattendant.updater.LocationUpdater;

import android.location.Location;
import android.os.Bundle;
import android.os.StrictMode;
import android.app.ListActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class AttendantViewerActivity extends ListActivity {
	private Report[] reports;
	private LocationUpdater updater;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder()
				.permitAll().build();
		StrictMode.setThreadPolicy(policy);
		updater = (LocationUpdater) getApplication();
	}

	@Override
	public void onResume() {
		super.onResume();
		updater.updateLocation();
		Location location = updater.getLocation();
		updateListView(location);
	}

	private void updateListView(Location location) {

		WSClientTask client = new WSClientTask(this, location.getLatitude(),
				location.getLongitude());
		client.execute();
		try {
			waitForCompletion(client);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reports = client.getReports();
		String[] reportsText = new String[reports.length];
		for (int i = 0; i < reportsText.length; i++) {
			reportsText[i] = "Tipo: " + reports[i].getType() + "\nHora: "
					+ reports[i].getTime().toString();
		}
		this.setListAdapter(new ArrayAdapter<String>(this,
				R.layout.activity_attendant_viewer, reportsText));
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		Intent intent = new Intent(this, DetailsActivity.class);
		intent.putExtra("report", reports[position]);
		startActivity(intent);
	}

	private void waitForCompletion(WSClientTask client)
			throws InterruptedException {
		while (!client.isComplete()) {
			Thread.sleep(50);
		}
	}

	@Override
	public boolean onOptionsItemSelected(final MenuItem item) {

		// Handle item selection
		switch (item.getItemId()) {

		case R.id.menu_refresh:
			updater.updateLocation();
			Location location = updater.getLocation();
			updateListView(location);
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.activity_attendant_viewer, menu);
		return true;
	}

}
